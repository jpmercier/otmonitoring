# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
# Filename: core.py
#  Purpose: Module to interact with drilling information
#   Author: OT Geotechnical Engineering Team
#    Email: OT_GDM
#
# Copyright (C) Rio Tinto / Oyu Tolgoi Mine
# --------------------------------------------------------------------
"""
Module to interact with drilling information

:copyright:
    Rio Tinto / Oyu Tolgoi Mine
:license:
    Exclusive use within Rio Tinto. The Licence is similar to BSD licence for
    RT internal use
"""



def read_transform(fname):
    """
    Read the transform from an Atlas Copco drilling file
    :param fname: File name
    :return: Transform matrix
    :rtype: numpy.array
    """
    import numpy as np
    Transform = np.zeros(9)
    for line in open(fname):
        if "Transform" in line:
            index = 0
            for k, value in enumerate(line.split()):
                if k in [1, 2, 3, 4, 5, 6, 7, 8, 9]
                    Transform[index] = float(value)
                    index += 1
            Transform = Transform.reshape(3,3)

    return Transform

def drill_projection(fname):
    """
    :param fname: File name
    :return: drill projection
    """
    BOOM = []
    active = False
    for k, line in enumerate(open(fname)):
        if 'BOOM' in line:
            active = True
            lno = k
        if active = True:
            BOOM.append((lno, float(line.split().strip()[0])))

Sub DrillProjection()
i = 16
Do While Cells(i, 1) <> ""

    AngleRands = (Cells(i, 6) / 10) * 0.0174533
    DirRands = 0
    PlaceHolderRands = (Cells(i, 5) / 10) * 0.0174533

    Old_X = 0
    Old_Y = 0
    Old_Z = Cells(i, 7) / 1000

    NewX = (Cos((AngleRands)) * Cos((DirRands))) * Old_X + (Cos((AngleRands)) * Sin((DirRands)) * Sin((PlaceHolderRands)) - Sin((AngleRands)) * Cos((PlaceHolderRands))) * Old_Y + (Cos((AngleRands)) * Sin((DirRands)) * Cos((PlaceHolderRands)) + Sin((AngleRands)) * Sin((PlaceHolderRands))) * Old_Z
    NewY = (Sin((AngleRands)) * Cos((DirRands)) * Old_X) + ((Sin((AngleRands)) * Sin((DirRands)) * Sin((PlaceHolderRands)) + Cos((AngleRands)) * Cos((PlaceHolderRands))) * Old_Y) + ((Sin((AngleRands)) * Sin((DirRands)) * Cos((PlaceHolderRands)) - Cos((AngleRands)) * Sin((PlaceHolderRands))) * Old_Z)
    NewZ = ((-Sin((DirRands))) * Old_X) + ((Cos((DirRands)) * Sin((PlaceHolderRands))) * Old_Y) + ((Cos((DirRands)) * Cos((PlaceHolderRands))) * Old_Z)


    Cells(i, 5) = NewX + Cells(i, 2)
    Cells(i, 6) = NewY + Cells(i, 3)
    Cells(i, 7) = NewZ + Cells(i, 4)

i = i + 1
Loop
End Sub



def read_atlas_copco(fname):
    """
    :param fname: File name
    :type fname: string
    :return: Drilling Object
    :rtype: OTMonitoring.Drilling.borehole
    """


