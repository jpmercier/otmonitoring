"""
This program calculate the average velocity (rate of change) over different
time periods (Hourly, Daily and Weekly).

The programs assumes a constant sampling rate. Should the sampling rate be
variable, the data will need to be resampled so the data are regularly
sampled. Resampling can be done in many different ways. The simplest is to
set the sampling interval to a constant value, for instance 1 second or 1
minute. Using the Pandas library, resampling can be achieved as follows:

 df.resample('1s')

where df is a Pandas DataFrame object and '1s' is a string that indicate the
new sampling rate (in this case 1s). Using the string '1T' would mean
resampling the data using a sampling interval of 1 minute.

NB: Given the resolution of the displacement measurements, unless the sampling
interval is large enough, the calculation of the instanteneous velocity (
calculation variation is displacement between two contiguous samples) will
mostly yield null values.

"""


import numpy as np
from datetime import datetime, timedelta
from scipy.ndimage.filters import gaussian_filter1d
import scipy.integrate as integrate
import pandas as pd
from scipy.stats import linregress
from scipy.optimize import curve_fit

def func(x, a, b):
    return a * x + b

import matplotlib.pyplot as plt

# function to calculate the rate
def rate_fun(y, si):
    """
    This function calculate the average rate of change or velocity by fitting a
    a linear trend line through a series of displacement measurements. It
    utilizes the linregress function from the scipy.stats library.
    :param y: the data vector
    :param si: sampling interval
    :return: the slope of the displacement curve
    """

    #  x is an array representing the time from the start of the interval. Its
    #  length is the same as y
    x = np.arange(0, len(y)) * si

    #  linregress function returns an 2 x 1 array. The first element is the
    # slope the second is the intercept.
    slope = linregress(x, y)
    return slope[0]

##########################################################
#  Making synthetic data, not relevant for calculations  #
##########################################################

si = 60.  # sampling interval in second
duration = 30  # duration in days

td = np.arange(0, duration * 24 * 3600, si)

starttime = datetime.now()
time = np.array([starttime + timedelta(0, t) for t in td])

# generating synthetic data
rate = gaussian_filter1d(np.abs(np.random.randn(len(time))), 100) / 1000
rate = rate #- #0.90 * np.mean(rate)
df_original = pd.DataFrame(rate / 60, index=time, columns=['original rate'])
trend = integrate.cumtrapz(rate, x=np.arange(0, len(rate)))
# integrating the trend in the

data = trend

tmp = {'data' : data, 'time': time[:-1]}

#  Creating a Pandas DataFrame
df = pd.DataFrame(tmp, index=time[:-1], columns=['data'])

##########################################################
#  End of making synthetic data                          #
##########################################################

# Calculating the rate of change average over different period of time

# 1 Weekly average

nsample_week = int(7 * 24 * 60 * 60 / si)  # number of "sampling_interval" in a
# week
rate_week = df.rolling(window=nsample_week).apply(lambda x : rate_fun(x, si))

# Renaming the column for plotting purposes
rate_week.rename(columns={'data': 'weekly average'}, inplace=True)


# 2 daily average

nsample_day = int(24 * 60 * 60 / si)  # number of "sampling_interval" in a day
rate_day = df.rolling(window=nsample_day).apply(lambda x : rate_fun(x, si))
rate_day.rename(columns={'data': 'daily average'}, inplace=True)

# 3 hourly average

nsample_hour = int(60 * 60 / si)  # number of "sampling_interval" in an hour
rate_hour = df.rolling(window=nsample_hour).apply(lambda x : rate_fun(x, si))
rate_hour.rename(columns={'data': 'hourly average'}, inplace=True)

# plotting results

ax = df_original.plot()
rate_week.plot(ax=ax)
rate_day.plot(ax=ax)
rate_hour.plot(ax=ax)
plt.xlabel('time')
plt.ylabel('rate [m/s]')
plt.title('rate of movement')
# df.plot(ax=ax)
plt.show()
